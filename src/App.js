import ProductList from "./components/ProductList";
import { CatalogueProvider } from "./Providers/catalogue";
import { CartProvider } from "./Providers/cart";
import { Container } from "./AppStyle.js";

function App() {
  return (
    <Container className="App">
      <CatalogueProvider>
        <CartProvider>
          <ProductList type="catalogue" />
          <ProductList type="cart" />
        </CartProvider>
      </CatalogueProvider>
    </Container>
  );
}

export default App;
