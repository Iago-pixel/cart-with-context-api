import Button from "../Button";

import { useContext } from "react";

import { CatalogueContext } from "../../Providers/catalogue";
import { CartContext } from "../../Providers/cart";

import { Container, List } from "./style";

// import { Container, List } from "./styles";

const ProductList = ({ type }) => {
  const { catalogue } = useContext(CatalogueContext);
  const { cart } = useContext(CartContext);

  return (
    <Container>
      {type === "catalogue" && <h1>Catalogue:</h1>}
      {type === "cart" && <h1>Cart:</h1>}
      <List>
        {type === "catalogue" &&
          catalogue.map((item, index) => (
            <li key={index}>
              <img alt="product" src={item.image} />
              {item.name} <Button type={type} item={item} />
            </li>
          ))}

        {type === "cart" &&
          cart.map((item, index) => (
            <li key={index}>
              <img alt="product" src={item.image} />
              {item.name} <Button type={type} item={item} />
            </li>
          ))}
      </List>
    </Container>
  );
};

export default ProductList;
