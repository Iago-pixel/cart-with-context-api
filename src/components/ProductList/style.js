import styled from "styled-components";

export const Container = styled.div`
  h1 {
    font-weight: lighter;
    margin: 5px;
  }
`;

export const List = styled.ul`
  display: flex;
  flex-wrap: wrap;
  padding: 0;
  @media (min-width: 570px) {
    padding: 1rem;
  }
  li {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 5px;
    border: 1px solid black;
    border-radius: 8px;
    padding: 1rem;
    width: 100px;
    height: 160px;
    font-weight: lighter;
    @media (min-width: 570px) {
      margin: 1rem;
    }
    img {
      width: 100%;
    }
  }
`;
