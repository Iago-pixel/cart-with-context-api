import styled from "styled-components";

export const Container = styled.button`
  font-weight: lighter;
  width: 100px;
  margin: 1rem;
  border: 1px solid black;
  border-radius: 8px;
  background-color: transparent;
  cursor: pointer;
  :hover {
    border-width: 2px;
    font-weight: normal;
  }
`;
