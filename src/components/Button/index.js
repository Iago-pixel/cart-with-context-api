import { useContext } from "react";

import { CartContext } from "../../Providers/cart";
import { CatalogueContext } from "../../Providers/catalogue";

import { Container } from "./style";

const Button = ({ type, item }) => {
  const { addToCart, removeFromCart } = useContext(CartContext);
  const { addToCatalogue, removeFromCatalogue } = useContext(CatalogueContext);

  const text = type === "catalogue" ? "Add to cart" : "Remove from cart";

  const handleClick = () => {
    if (type === "catalogue") {
      removeFromCatalogue(item);
      addToCart(item);
    } else {
      removeFromCart(item);
      addToCatalogue(item);
    }
  };

  return <Container onClick={handleClick}>{text}</Container>;
};

export default Button;
