import styled from "styled-components";

export const Container = styled.div`
  padding: 0 5px;
  @media (min-width: 570px) {
    padding: 1rem;
  }
`;
